def dechiffrer_lettre(cle: int, lettre: str):
    if 65 <= ord(lettre) <= 90:
        return chr((ord(lettre) - 65 - cle) % 26 + 65)

    elif 97 <= ord(lettre) <= 122:
        return chr((ord(lettre) - 97 - cle) % 26 + 97)

    return i

TEXTE_A_DECHIFFRER = """ INSERER TEXTE ICI """

resultat = ""

for i in TEXTE_A_DECHIFFRER:
    resultat += dechiffrer_lettre(INSERER_CLE, INSERER_LETTRE)

# Affiche le résultat à l'écran
print(resultat)

# Présentation

Une vidéo de présentation du sujet peut être trouvée
[ici](https://drive.google.com/file/d/1WR_b2FNNj6Wl91UH-h6WHRz1l63ptAGP/view?usp=drive_link).

Et le drive avec tous les documents, [ici](https://drive.google.com/drive/folders/1l2u2dZeWgqnYGo7G18P2HFAkVAm3TrOq)

# Warning

Le zip dans les given resources est chiffré !
Mot de passe du ZIP : PROLOKING

# Les réponses

Le site à trouver est https://osm.prologin.org.
Il est dans le document chiffré.


Sur le site, les identifiants des différentes étapes sont : 

email: admin@osm.evil,  	mdp: 0SmAdm1Ndp
email: mfontaine@osm.evil,  mdp: LeMdP0SmTaUp3

Le mot de passe de mfontaine, est sur un bout de papier qui est drop par la
taupe après qu'ils aient reconnu mfontaine.
Les infos qui doivent être notées sur le bout de papier : 

```
Commence par un article défini parce qu'il est unique au monde
Contient 'mdp' parce que c'est un Mot De Passe
Est suivi du nom de l'organisation
Fini par le nom d'un animal incroyable
Les majuscules alternent
Tous les 'o' sont remplacés par des '0'
Tous les 'e' majuscules sont remplacés par des '3'
Tous les 'a' majuscules sont remplacés par des '4'
```

Le résultat est donc : LeMdP0SmTaUp3

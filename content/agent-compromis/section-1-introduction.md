---
show_toc: false
---

Voici les documents retrouvés sur l'ordinateur de Joseph.

Bonne chance.

PS : On a trouvé aussi cette brochure publicitaire :

{{<figure src="./resources/images/avion_1.jpg" height=100% width=100% alt="Avion 1">}}
{{<figure src="./resources/images/avion_2.jpg" height=100% width=100% alt="Avion 2">}}

---
title: "Agent Compromis"
authors: ["Julie Fiadino", "Gregoire Lefaure", "Clément Nguyen", "Julie Durandeau"]
description: |
    Fichiers de l'ordinateur de Joseph

code_stub_url: "./resources/given_resources/00 011 111010 10111 111010 111001 111000 1101 111111.zip"

hidden: true
layout: multiple_sections
---

# Étape 2 : Affichage basique

{{% box type="danger" title="Objectif" %}}

Avec notre nouvel écran, nous allons afficher quelques informations sur l'état
de notre serre.

{{% /box %}}

{{% box type="info" title="Rappel" %}}

Pour récuperer la température, on utilise la fonction suivante :

```python
input.temperature()
```

Pour l'humidité, on utilise :

```python
pins.analog_read_pin(AnalogPin.P1)
```

<br>

N'oubliez pas de mettre en pause votre programme avec la ligne suivante :

```python
basic.pause(ms)
```

{{% /box %}}

{{% box type="exercise" title="Température et humidité" %}}

Affichez les informations de température et d'humidité toutes les
secondes.

{{% /box %}}

# Étape 4 : Choix de programme personnalisé

{{% box type="danger" title="Objectif" %}}

Le deuxième cycle simulera une journée où les modules s’activent à de certaines
heures.

{{% /box %}}

Quand le cycle est sélectionné, “OFF” sera affiché. Si on appuie sur le bouton
logo, le cycle se lance.

Nous considérons qu'une heure est égale à 1 seconde. On va alors utiliser le
principe du sujet de la semaine dernière.

{{% box type="exercise" title="Interrupteur basique" %}}

On va compter le nombre d’appuis sur le bouton A et le nombre d’appuis sur le
bouton B pour définir ces principes :

- le nombre d’appuis sur le bouton A correspond au temps pendant lequel la
pompe est allumée ;
- le nombre d’appuis sur le bouton B correspond au temps pendant lequel la
pompe est éteinte.

Au total, nous devons avoir 24 nombre d’appuis sur le bouton A et le bouton B.
Tant qu’on n’a pas atteint ces 24 appuis, vous devez vérifier les entrées sur
les boutons.

Par la suite, tu devras faire une itération du cycle de la pompe. Vous pouvez
alors reprendre le code que vous aviez fait la séance précédente.

{{% /box %}}

Sur l'écran, à la place de "OFF" le nombre de jours passés s’affiche. Au début,
il y a écrit "0 jour", une fois qu’un cycle se termine, l’affichage passe à
"1 jour" et ainsi de suite.

{{% box type="info" title="Rappel" %}}

N'hesitez pas à revenir dans les autres TPs et le début de ce dernier pour vous
aider à trouver les fonctions dont vous avez besoin.

{{% /box %}}

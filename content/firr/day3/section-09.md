# Étape 5 : Utilisons notre serre

{{% box type="danger" title="Objectif" %}}

Il est temps d'utiliser notre nouvelle serre !

{{% /box %}}

Nous allons planter de la ciboulette, plante aromatique essentielle pour la
santé mentale de nos futurs martiens.

Demandez de l'aide aux organisateurs pour mettre en place votre serre !

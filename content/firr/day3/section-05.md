# Étape 4 : Choix de programme personnalisé

{{% box type="danger" title="Objectif" %}}

Nous allons créer un menu permettant de sélectionner le mode de fonctionnement
de serre que nous voulons.

{{% /box %}}

Il y aura 3 modes :

1. Le premier sera le mode manuel : s'il est sélectionné, c’est à l'utilisateur
de choisir quand arroser, éteindre et allumer les LEDs.
2. Le deuxième sera un mode "simulation du jour" : l’arrosage et la lumière
s’activeront pendant un temps défini.
3. Le dernier mode est adaptatif : il s'adaptera aux conditions météorologiques
du jour.

Il faut d’abord concevoir le menu de sélection du programme. Quand la serre
s’allume, il faut sélectionner le mode. Sur l'écran de base, il y aura écrit 
"Manuel", si on appuie sur le bouton de droite, il y a écrit "Cycle Jour", si
on reappuie sur le bouton de droite, il y a écrit "Adaptatif". Si on appuie
encore une fois, on revient à l'écran "Manuel". 

Le bouton de gauche permet de faire tourner les cycles dans l’autre sens. 
Pour sélectionner le cycle, il faut appuyer sur le logo bouton.

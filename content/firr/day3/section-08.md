# Étape 4 : Choix de programme personnalisé

{{% box type="danger" title="Objectif" %}}

Le dernier cycle sera aussi automatique. Il s'adapte aux conditions météo. Cette
partie est un bonus !

{{% /box %}}

Comme pour le cycle précédent, au début, l'écran affiche "OFF". Lorsqu'on lance
le cycle, nos deux modules sont automatiques. La bande de LEDs s’active si la
lumière est en dessous d’un certain seuil. Notre module d’arrosage s’activera
pendant 2 secondes lorsque le capteur d'humidité passera en dessous d’un certain
seuil.

Pendant que le cycle est actif, l'écran doit afficher le nombre d’arrosage
possibles avant que le réservoir soit vide. Par exemple, au début du cycle,
l'écran affiche : "Restants : 5/5". Quand un arrosage se lance, ce chiffre
descend à 4/5. Lorsqu’il atteint "0", un message d’alerte s’active pour
demander de remplir le réservoir.

{{% box type="info" title="Rappel" %}}

N'hesitez pas à revenir dans les autres TPs et le début de ces derniers pour
vous aider à trouver les fonctions dont vous avez besoin.

{{% /box %}}

```text {nocopy = true}
Projet : Colonisation de mars
Division : Systèmes de vie martien 
Equipe : FIRR (Futurs Ingénieurs de René Rollin)
Date : 12 avril 2024 - Mission 3
```

<br>

Après avoir établi un système d'éclairage et d'arrosage fonctionnel pour notre
**serre martienne**, il est temps de passer à l'étape de la communication et du
contrôle visuel. 

{{<figure src="resources/images/firr.png" alt="Logo FIRR">}}

Dans cette mission, vous allez utiliser un écran LCD pour afficher des
informations clées sur l'état de la serre, offrant ainsi une interface
utilisateur **intuitive** pour les astronautes sur Mars. Vous allez apprendre à
programmer cet écran pour afficher des données variables, intéragir avec le
système et offrir un retour en temps réel sur l'état de la serre.

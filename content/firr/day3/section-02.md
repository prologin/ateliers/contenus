# Étape 1 : Installation du module LCD et test

{{% box type="danger" title="Objectif" %}}

Il faut tout d'abord installer et tester notre nouveau système dans notre serre
martienne. 

{{% /box %}}

Branchez le module sur le PIN restant et affichez votre premier message.

{{% box type="info" title="Mise en place de l'écran" %}}

Il faut d'abord allumer notre écran LCD ! Pour cela, vous pouvez utiliser les
deux lignes ci-dessous :

```python
grove.lcd_init()
grove.lcd_dispaly_on()
```

Ensuite, pour afficher du texte ou des nombres, il vous faut utiliser :

```python
grove.lcd_show_string(texte, x, y)
grove.lcd_show_number(nombre, x, y)
```

Pour chaque foncton, nous avons 3 paramètres. Pour `texte`, il est important de
mettre notre texte entre guillemets. Les parametres `x` et `y` permettent de
déplacer le texte sur l'écran.

{{% /box %}}

{{% box type="exercise" title="Premier test" %}}

Essayez d’afficher le message "Bienvenue sur Mars !"

{{% /box %}}

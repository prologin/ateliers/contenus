# Étape 3 : Utilisation des boutons

{{% box type="danger" title="Objectif" %}}

Programmer le micro:bit pour utiliser les boutons afin de naviguer sur notre
écran. Cette fonctionnalité permettra une intervention rapide et une flexibilité
dans la gestion de la serre.

{{% /box %}}

{{% box type="info" title="Rappel" %}}

Avant toute chose, il faut définir le comportement des boutons. Pour cela, on
utilisera l'exemple ci-dessous pour définir le comportement du bouton A, ici,
afficher "Hello Mars!" :

```python
def on_button_pressed_a():
    basic.show_string("Hello Mars !")
input.on_button_pressed(Button.A, on_button_pressed_a)
```

{{% /box %}}

{{% box type="exercise" title="Interrupteur basique" %}}

En utilisant le même principe, essayez de reprogrammer les boutons pour que le
bouton de gauche affiche la température et que celui de droite affiche
l'humidité sur l'écran.

{{% /box %}}

Ensuite, nous allons programmer un système d'arrosage grâce à l'écran. De base,
sur l'écran va être vide. Si on appuie sur le bouton de droite, une barre de
chargement commencera à se remplir petit à petit sur l'écran. Si on appuie sur
le bouton de gauche, la barre se videra.

Une fois que l'utilisateur a choisi son niveau d'arrosage, il appuiera sur le
bouton logo. Voici quelques exemples :

- si la barre est vide, il n'y a pas d'arrosage ;
- si la barre est pleine, l'arrosage durera 4 secondes ;
- si la barre est remplie à moitié, l'arrosage dure 2 seconde.

{{% box type="exercise" title="Niveau d'arrosage" %}}

Essayez de réaliser ce système d'arrosage. La première étape et de réaliser la
barre de remplissage sur l'écran, une variable doit être utilisée pour retenir
à combien la barre est remplie. La deuxieme étape est d'activer le systeme
d'arrosage pendant la bonne durée quand le bouton logo est pressé.

{{% /box %}}

{{% box type="info" title="Rappel" %}}

Pour détecter si le bouton logo est pressé, on utilise :

```python
input.logo_is_pressed()
```

Rappelez-vous que c'est une condition, il doit donc être utilisé avec une
condition.

Pour allumer ou éteindre la pompe, la fonction à utiliser est :

```python
pins.analog_write_pin(pin, valeur)
```

Ici, notre fonction a 2 paramètres : `pin` et `valeur`.
Pour le premier paramètre, nous utiliserons toujours : `AnalogPin.P2`.
Pour `valeur`, il vous faut mettre 1023 pour allumer la pompe et 0 pour
l'éteindre.

{{% /box %}}

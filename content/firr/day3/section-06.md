# Étape 4 : Choix de programme personnalisé

{{% box type="danger" title="Objectif" %}}

Réalisons le premier cycle. Ce cycle sera le mode "Manuel".

{{% /box %}}

Une fois que le cycle est sélectionné, deux nouveaux menus apparaissent :
“Lumière” et “Arrosage”. On navigue entre les paramètres comme pour la sélection
des cycles.

Lorsque le menu "Lumière” apparaît, si on appuie sur le bouton logo, les
lumières s’allument ou s'éteignent.

Lorsque le menu "Arrosage" apparaît, tant que l’utilisateur maintient le
bouton, la pompe est active.

{{% box type="info" title="Rappel" %}}

N'hesitez pas à revenir dans les autres TPs et le début de ces derniers pour vous
aider à trouver les fonctions dont vous avez besoin.

{{% /box %}}

---
title: "À propos"
hidden: true
toc: disable
---

# Les Ateliers Prologin

Les Ateliers Prologin sont des interventions proposées aux établissements 
scolaires. Elles vont de la simple découverte de la programmation pour les plus
petits, à l'application de notions vues dans les cours de SNT/NSI, avec des 
applications concrètes comme la cryptologie, la cybersécurité, le réseau ou le 
web par exemple. Les ateliers ont lieu pendant le temps scolaire, en 
collaboration avec les professeurs, allant du CM1 à la Terminale.

Les sujets sont créés par des bénévoles de l’association Prologin et sont prévus
pour durer entre 1 et 3 séances suivant le sujet.

Vous pouvez trouver ci-dessous une description des différents ateliers qui
existent actuellement.

Par ailleurs, n'hésitez pas à nous contacter à
[info@prologin.org](mailto:info@prologin.org) pour plus d'information !

# Que proposons-nous ?

## Agent Compromis

À l'occasion d'un escape game, les élèves doivent aider le Bureau Fédéral
d'Enquête à démasquer une taupe et retrouver leur agent disparu ! Pour ce faire,
les élèves vont découvrir des techniques d'OSINT, du Python, et même hacker
un site web afin de retrouver la taupe et l'agent.

<u>Temps :</u> ~1h30  
<u>Classe :</u> 4e - Terminale

## Scratch

Cet atelier permet de découvrir le concept de programmation en animant son
prénom avec [Scratch](https://scratch.mit.edu) : lettres qui bougent, qui
dansent et qui chantent sur demande.

<u>Temps :</u> ~30 - 45min  
<u>Classe :</u> CM1 - CM2

## Serres connectées

Au travers d'un projet se déroulant sur 3 à 4 séances, les élèves découvrent
Python lors de la construction d'une mini serre connectée : arrosage et
éclairage automatique, écran de suivi et d'information. En bref, tout le
nécessaire pour faire pousser ses graines en (presque) complète autonomie.

<u>Temps :</u> 3 séances d'1h30 - 2h  
<u>Classe :</u> 4e - Seconde  
<u>Lien :</u> [https://ateliers.prologin.org/firr](https://ateliers.prologin.org/firr/)

## Chifoumicro:bit

Les élèves découvrent la programmation en Python au travers d'un projet ludique :
coder un chifoumi revisité sur une carte `micro:bit`.

<u>Temps :</u> 1h - 1h30  
<u>Classe :</u> 5e - 3e  
<u>Lien :</u> [https://ateliers.prologin.org/chifoumicrobit](https://ateliers.prologin.org/chifoumicrobit/)

## Web

Au cours de cet atelier, les élèves font leurs premiers pas dans le développement
web en créant leur propre blog en HTML, et en lui donnant l'apparence de
leur choix avec du CSS.

<u>Temps :</u> 1h30  
<u>Classes :</u> 6e - Seconde  
<u>Lien :</u> [https://ateliers.prologin.org/web](https://ateliers.prologin.org/web)

## Prolobot

Durant cet atelier, les élèves découvrent les bases de la programmation et de
la robotique, en commandant un petit robot motorisé.

<u>Temps :</u> ~1h  
<u>Classe :</u> 6e - 3e  
<u>Lien :</u> [https://ateliers.prologin.org/microbit_prolobot](https://ateliers.prologin.org/microbit_prolobot/)

## Introduction à Python

Les élèves découvrent les bases de Python en réalisant des projets amusants tels
que le Juste Prix ou le Morpion.

<u>Temps :</u> 1h30 - 2h par projet  
<u>Classe :</u> 5e - 2nde  
<u>Lien :</u> [https://ateliers.prologin.org/introduction_python](https://ateliers.prologin.org/introduction_python/)

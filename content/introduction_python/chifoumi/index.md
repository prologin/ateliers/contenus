---
title: Le Duel des Éléments
authors: Amélie 'kicko' Bertin
date: 2025
description: "Découvre les bases de Python en codant le célèbre jeu du Pierre-Feuille-Ciseaux"
layout: multiple_sections

showcase: ./chifoumi-showcase.webp
tags:
    - Python
---

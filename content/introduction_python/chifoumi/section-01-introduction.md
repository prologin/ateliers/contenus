# Qu'est-ce que Python ?

{{% box type="warning" title="Objectifs" %}}

Au cours de cette découverte du langage Python, nous allons voir plusieurs
concepts :

- Afficher du texte ;
- Récupérer et utiliser le texte entré par l’utilisateur ;
- Utiliser des variables pour stocker des informations ;
- Comparer des nombres avec les opérateurs ;
- Exécuter du code sous certaines conditions ;
- Exécuter du code en boucle.

{{% /box %}}

**Python** est un des **langages de programmation** les plus populaires qui permet de
faire énormément de choses comme par exemple, des intelligences artificielles,
de la reconnaissance d'image ou des jeux vidéos.

Un langage de programmation permet au programmeur de donner des consignes à
l'ordinateur pour son programme.

On appelle ces consignes des **instructions**. Elles sont écrites à la suite dans
un fichier que lira l'ordinateur.

Les instructions sont exécutées à la suite, ligne par ligne.


## Blocs de code

Tout au long du TP, tu verras des blocs comme celui-ci :

```codepython
print("Hello world!")
```

Dans les boîtes noires, tu peux lancer le code écrit à l'intérieur ! Pour cela,
clique sur le bouton `Run`.

Tu peux également modifier le code à l'intérieur. N'hésite surtout pas à
modifier le code à l'intérieur du bloc pour avoir un autre comportement !


{{% box type="warning" title="Que faire ?" %}}

Passe à la page suivante, pour le début de la programmation !

Si tu ne comprends pas une partie, les organisateurs sont toujours disponibles
pour toi, appelle-les en levant la main !

{{% /box %}}

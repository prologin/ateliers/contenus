# Le Duel des Éléments

{{% box type="info" title="Scénario du jeu" %}}

**Contexte :**  
Deux amis, Alex et Sam, se retrouvent après l'école pour tuer le temps.
Ils sont assis à une table de café, discutant de tout et de rien, quand une
dispute amicale éclate sur qui est le plus chanceux.

**Personnages :**  
- **Alex** : Dynamique et compétitif(ve), adore les défis.
- **Sam** : Plus calme, mais aime prouver qu'il/elle a raison.

---

### Scène 1 : Le Défi
(*Les deux amis sont assis à une table, sirotant un soda. Alex pose son verre et fixe Sam avec un sourire en coin.*)

**Alex** : Hé, Sam, on a toujours su que j'étais plus chanceux(se) que toi, non ?  
**Sam** : Ha ! La chance, c’est pour ceux qui ne savent pas jouer !  
**Alex** : Oh vraiment ? Alors, prouvons-le ! Un bon vieux duel de Pierre-Feuille-Ciseaux !  
**Sam** : Classique ! Mais attention, je suis imbattable !  
(*Ils se penchent vers la table, les mains prêtes à jouer.*)

---

### Scène 2 : Rappel des Règles

**Alex** : Juste pour être sûr(e), on est d’accord sur les règles ?  
**Sam** : Bien sûr, mais vas-y, rappelle-les quand même.  
**Alex** : Facile :
- La pierre écrase les ciseaux.
- La feuille recouvre la pierre.
- Les ciseaux coupent la feuille.

**Sam** : Et si on fait le même choix ?  
**Alex** : Égalité, on recommence.  
**Sam** : Très bien ! Premier à trois victoires ?  
**Alex** : C’est parti !  
(*Ils joignent leurs poings et commencent à compter en rythme : "Pierre... Feuille... Ciseaux !" La partie commence !*)


---

### Conclusion Possible :
- **Alex gagne** : "Hah ! La chance était bien de mon côté !"
- **Sam gagne** : "Tu vois ? Ce n’est pas qu’une question de chance, c'est une stratégie !"
- **Égalité** : "On remet ça une prochaine fois !"

{{% /box %}}

Le personnage de Sam dans le scénario sera joué par l'ordinateur, alors que
le personnage de Alex sera joué par l'utilisateur de notre programme.

---

Pour créer notre jeu, nous pouvons décomposer notre programme en plusieurs
étapes.

## Définir le choix de l'ordinateur

Pour que le jeu soit fun à jouer, il faut que le choix de l'ordinateur, entre
Pierre Feuille et Ciseaux, soit aléatoire, et non tout le temps le même.

En Python, il est possible de générer un **nombre aléatoirement**, comme le
montre l'exemple suivant :

```codepython
from random import randint

# Génère un nombre aléatoirement entre 0 et 5 compris
nombre_aleatoire = randint(0, 5)

# Affiche le nombre généré
print(nombre_aleatoire)
```

Dans cet exemple, on génère un nombre aléatoire entre 0 et 5, dont les
possibilités sont : 0, 1, 2, 3, 4 et 5.

---

Pour le jeu Pierre-Feuille-Ciseaux, nous avons 3 possibilités.  
Définissons à quoi correspondent les valeurs :
- 1 : Pierre
- 2 : Feuille
- 3 : Ciseaux

{{% box type="exercise" title="Générer le choix de l'ordinateur" %}}

À toi de générer un nombre aléatoire entre 1 et 3 et stocke le résultat
dans une variable qui se nomme `choix_ordi`.

{{% /box %}}

## Créer un score

Les deux amis veulent savoir qui est le meilleur au bout de 3 victoires.

On veut donc compter le nombre de victoires de chaque joueur.

{{% box type="exercise" title="Créer notre score" %}}

Crée 2 variables nommées `score_ordi` et `score_joueur` qui valent au début 0.

{{% /box %}}

À chaque fois qu'un joueur bat l'autre, l'un des scores augmentera.

## Récupérer une entrée du joueur

Maintenant que l'on a choisi pour l'ordinateur, l'utilisateur va devoir
choisir également.

{{% box type="exercise" title="Demander le choix de l'utilisateur" %}}

Pour récupérer le choix de l'utilisateur, tu peux utiliser la fonction
`input()`.

Il faudra stocker le choix du joueur, en tant que nombre, dans la variable
`choix_joueur`.

{{% /box %}}

{{% box type="warning" title="Convertir l'entrée en nombre" %}}

Attention, il faudra que tu transformes l'entrée utilisateur en nombre.
Pour cela, tu peux utiliser la fonction `int()` :
```codepython
text = "10"
nombre = int(text)
print(nombre + 1)
```

{{% /box %}}

## Qui gagne le round ?

Maintenant que les 2 choix sont faits, nous pouvons déterminer qui a gagné ce tour.

Pour rappel :
- La pierre écrase les ciseaux.
- La feuille recouvre la pierre.
- Les ciseaux coupent la feuille.

Et nous avons défini les correspondances suivantes :
- 1 : Pierre
- 2 : Feuille
- 3 : Ciseaux

Donc pour qu'une personne gagne, nous avons :
- 1 bat 3
- 2 bat 1
- 3 bat 2  
(1 > 3 > 2 > 1)

Nous avons 3 cas à prendre en compte :
- Si les choix de l'ordi et du joueur sont **égaux**, alors c'est une égalité
- Sinon, si l'un des choix est égal à 1 et l'autre est égal à 3 (**Pierre vs
Ciseaux**), alors la personne qui a fait Pierre gagne
- Sinon, si l'un des choix est supérieur à l'autre (**Ciseaux vs Feuille**, ou
**Feuille vs Pierre**), alors le joueur qui a fait le choix le plus élevé gagne.


{{% box type="exercise" title="Jouer un round" %}}

Affiche une phrase pour dire qui a gagné ce round !  
Tu peux utiliser les **conditions**, vues plus tôt dans l'atelier.  
Tu utiliseras les mots-clés `if`, `elif` et `else`.

De plus, si l'ordinateur gagne, ajoute 1 à la variable `score_ordi`.  
Si c'est le joueur qui a gagné, augmente la variable `score_joueur`.

{{% /box %}}

## Continuer le jeu jusqu'à la 3e victoire

{{% box type="info" title="Répéter des instructions" %}}

En programmation, il est possible de répéter des instructions plusieurs fois.  
On appelle cela des **boucles**.  
En particulier, il existe un type de boucle qui va exécuter un bout de code
**tant que** une condition est vérifiée (tant qu'elle est vraie) : les boucles
`while`.

Voici un petit exemple des boucles :

```codepython
nombre = 0

# Tant que `nombre` est inférieur à 5
# on répète le bout de code décalé à droite
while nombre < 5 :
    nombre = nombre + 1
    print("Mon nombre est égal à :", nombre)

print("À la fin, mon nombre est égal à :", nombre)
```

Dans cet exemple, l'ordinateur va répéter les instructions `nombre = nombre + 1`
et son affichage tant que la variable `nombre` est inférieure à 5.

{{% /box %}}

Dans notre jeu du Pierre-Feuille-Ciseaux, nous voulons jouer plusieurs tours,
jusqu'à ce qu'un joueur ait gagné 3 fois.

Nous voulons donc répéter les étapes :
- Choisir un nombre pour `choix_ordi` ;
- Récupérer l'entrée du joueur pour `choix_joueur` ;
- Vérifier qui a gagné le tour ;
- Mettre à jour les scores.

{{% box type="exercise" title="Bouclons !" %}}

À toi de rajouter la boucle `while` à l'endroit correspondant, de manière
d'avoir les bouts de code voulus dans notre boucle.

Il faut s'arrêter si `score_joueur` est égal à 3, ou `score_ordi` est égal à 3.

{{% /box %}}

{{% box type="info" title="Ouuuuuu" %}}

Pour réaliser un "ou" logique en Python, tu peux utiliser le mot-clé `or`,
comme ceci :
```codepython
nombre = 0 # change la valeur

if nombre < 5 or nombre > 10:
    print("Mon nombre est égal à :", nombre)

print("En fait, mon nombre c'est :", nombre)
```

Le "ou" logique renvoit vrai si :
- soit la 1ere condition est vraie,
- soit la 2nde condition est vraie,
- soit les 2 conditions sont vraies.

Change la valeur de `nombre` dans l'exemple, avec 1, 6, 8, 12.

{{% /box %}}

---

C'est la fin ! Tu devrais avoir un Pierre-Feuille-Ciseaux fonctionnel et
opérationnel pour que tu puisses y jouer !

Appelle un organisateur pour en découvrir plus sur la programmation.

# Début de la programmation

## Afficher du texte

Pour dire à l'ordinateur d'afficher du texte, tu peux utiliser
l'instruction suivante :

```codepython
print("Hello world!")
```

Ici, on a une instruction : `print` (*imprimer* en 🇫🇷).  
Elle permet d'afficher à l'écran le texte spécifié entre les guillemets `"`.

{{% box type="exercise" title="Mini-mission 1 : Allo ?"%}}

Julie est dans l'immeuble d'en face et tu souhaites lui envoyer un message !
Vous aviez mis en place un système pour communiquer entre vous avec du code
Python !

Essaye de lui envoyer "Chicken attack!".  
Pour cela, tu peux utiliser la fonction `print()`.

```codepython
# Affiche "Chicken attack!" après cette ligne !
```

{{% /box %}}

## Lire du texte

En Python, tu peux également récupérer le texte écrit par l'utilisateur,
en utilisant le code suivant :

```codepython
input("Quel jour est-il ?")
```

Tout comme la fonction `print()`, tu peux demander à l'ordinateur d'afficher
un texte avant de demander à l'utilisateur une entrée.

{{% box type="exercise" title="Mini-mission 2 : Réponse !"%}}

Julie essaye de te répondre à l'oral mais elle est trop loin... Pour remédier à
ce problème, demande lui une entrée pour comprendre ce qu'elle souhaite te
dire !

Demande une réponse de Julie avec comme message "Keske t'as dit ?".  
Pour cela, tu peux utiliser la fonction `input()`.

```codepython
# Demande à Julie "Keske t'as dit ?" après cette ligne !
```

{{% /box %}}


L'instruction `input` (*entrée* en 🇫🇷) nous permet de faire cette demande à
l'utilisateur.  
Cependant, on ne peut pas utiliser la réponse. Pour cela, nous la devons
stocker dans une variable (que nous voyons dans la partie suivante).

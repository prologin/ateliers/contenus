## Variables

Une **variable** est un élément qui associe un nom à une valeur.

```python {nocopy=true}
ma_variable = 2
```

On peut lui donner n'importe quel nom :
```python {nocopy=true}
variable = 1
toto = 10
julie = 100000
voiture = -1
toto_et_tata_et_tous_les_autres = 5
```

On peut ensuite utiliser cette variable dans le reste du programme, en utilisant son nom :
```codepython
age = 10
print(age)
print(age * 2)
print(age + 5)
```

Il est également possible de changer la valeur d'une variable :
```codepython
age = 10
print(age)
age = 12
print(age)
age = age + 3
print(age)
```

{{% box type="exercise" title="Mini-mission 3 : Somme"%}}

Le canal de discussion avec Julie a été coupé !  
Pour trouver la nouvelle fréquence, change la valeur de la variable `frequence`,
en ajoutant la valeur de fréquence donnée par Julie `difference`.

```codepython
frequence = 12
difference = 5

# mets ton code ici

print(frequence)
```

{{% /box %}}

### Les types

Une variable peut avoir une valeur de différents types :
- Un nombre entier : `ma_variable = 5`
- Une chaîne de caractères : `ma_variable = "bonjour !"` (on reconnait une telle variable grâce aux guillemets)
- Un nombre à virgule : `ma_variable = 1.05` (on utilise un point et non une virgule)
- Un booléen : `ma_variable = True` (deux valeurs possibles : `True` ou `False`, vrai ou faux, bien penser à mettre la majuscule !)

---

Tu peux maintenant stocker la réponse à une instruction `input` dans une variable :

```codepython
prenom = input("Comment t'appelles-tu ?")
print(prenom)
```

L'instruction `input` renvoie le texte entré par l'utilisateur.
Et nous stockons ce texte dans la variable `prenom`, qui est ensuite affiché grâce à l'instruction `print`.

# Qu'est-ce que Python ?

{{% box type="danger" title="Objectifs" %}}

Au cours de cette découverte du langage Python, nous allons voir plusieurs
concepts :

- Afficher du texte ;
- Récupérer et utiliser le texte entré par l’utilisateur ;
- Utiliser des variables pour stocker des informations ;
- Faire des calculs et comparer des nombres avec les opérateurs ;
- Exécuter du code sous certaines conditions ;
- Exécuter du code en boucle.

{{% /box %}}

**Python** est un des langages de programmation les plus populaires qui permet de
faire énormément de choses comme par exemple, des intelligences artificielles
de la reconnaissance d'image ou des jeux vidéos.

Un langage de programmation, c'est comme un moyen de communication entre
l'Homme et les ordinateurs. Cela permet au programmeur de donner des
consignes à l'ordinateur pour son programme.

On appelle ces consignes des **instructions**. Elles sont écrites à la suite dans
un fichier que lira l'ordinateur. Les instructions sont exécutées à la suite
des autres, ligne par ligne.

{{% box type="warning" title="Comment on fait ?" %}}

Les prochaines sections te montrent les fonctionnalités de Python qui te seront
utiles pour créer ton projet par la suite. N’hésite pas à essayer par toi-même
les exemples donnés pour voir comment ça fonctionne !

Si tu ne comprends pas une partie, les organisateurs sont toujours disponibles
pour toi !

{{% /box %}}

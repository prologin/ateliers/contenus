# Le jeu du juste prix

{{% box type="danger" title="But du jeu" %}}

Le **Juste Prix** est de faire deviner un nombre entre 1 et 1000 à Julie. Elle
commence en entrant un nombre dans le programme, et le programme doit répondre
"Plus" si le nombre est plus petit que celui à deviner ou "Moins" s’il est plus
grand.

Le jeu se termine jusqu'à ce que Julie arrive à trouver le bon nombre. Son score
sera le nombre de fois que Julie a rentré une réponse.

{{% /box %}}

Pour créer notre jeu, nous pouvons décomposer notre programme en plusieurs
étapes.

## Choisir un nombre entre 1 et 1000

Il va nous falloir créer notre nombre aléatoire entre 1 et 1000. Ce dernier sera
le nombre à deviner pour Julie.

{{% box type="info" title="Comment on fait de l'aléatoire ?" %}}

Pour générer un nombre aléatoire en Python, on va utiliser des programmes déjà
existants. Tu peux utiliser le programme suivant :

```codepython
from random import randint

# Génère un nombre aléatoirement entre 0 et 5 
nombre_aleatoire = randint(0, 5)

# Affiche le nombre généré
print(nombre_aleatoire)
```

Dans cet exemple, on génère un nombre aléatoire entre 0 et 5. Ainsi, les nombres
possibles sont : 0, 1, 2, 3, 4 et 5.

{{% /box %}}

{{% box type="exercise" title="Générer notre nombre caché" %}}

À toi de générer un nombre aléatoire entre 1 et 1000 pour que Julie puisse le
trouver ! Tu devras alors le stocker dans une variable qui se nomme
`nombre_aleatoire`.

{{% /box %}}

## Créer un score

Pour garder l'avancement de Julie dans le jeu du Juste Prix, nous allons
utiliser une variable. Au tout début de notre programme, cette variable sera
égale à 0.

{{% box type="exercise" title="Créer notre score" %}}

Crée une variable nommée `score` qui vaut au début 0.

{{% /box %}}

À chaque fois que Julie fera une proposition, ce score augmentera.

## Récupérer une entrée du joueur

Maintenant que l'on a choisi un nombre entre 1 et 1000, Julie va devoir le
deviner en proposant des nombres.

{{% box type="warning" title="Mettre à jour notre score" %}}

Il ne faut pas oublier d'ajouter 1 au score pour prendre en compte la nouvelle
proposition de Julie.

{{% /box %}}

{{% box type="exercise" title="Demander un nouveau nombre" %}}

Il va falloir demander à julie un nouveau nombre à comparer avec notre nombre
généré. Pour cela, tu peux utiliser la fonction `input()` pour demander une
entrée à Julie.

Attention, il faudra que tu transformes cette entrée en nombre. pour cela, tu
peux utiliser la fonction `int()`. Il faudra stocker le tout dans la variable
`nombre`.

{{% /box %}}

## Vérifier l'entrée du joueur

Une fois que l'entrée de Julie a été récupérée, il faut comparer le nombre donné
au nombre aléatoire généré précédemment. Nous avons alors trois cas à prendre
en compte :

- Si `nombre` et `nombre_aleatoire` sont <ins>égaux</ins>, il faut afficher **"Gagné !"** ;
- Si `nombre` est <ins>supérieur</ins> à `nombre_aleatoire`, il faut afficher **"Moins !"** ;
- Si `nombre` est <ins>inférieur</ins> à `nombre_aleatoire`, il faut afficher **"Plus !"**.

{{% box type="exercise" title="Vérifier le nombre donné" %}}

Pour afficher la bonne phrase, tu peux utiliser les **conditions** que tu as vu
plus tôt dans l'atelier. Tu utiliseras les mots-clés `if`, `elif` et `else`.

{{% /box %}}

## Continuer le jeu tant que le joueur ne trouve pas le nombre aléatoire

{{% box type="info" title="Répéter des instructions" %}}

En programmation, il existe un moyen d'exécuter un certain nombre de fois
plusieurs instructions. On appelle cela des **boucles**. En particulier, il existe
un type de boucle qui va exécuter un bout de code **tant que** une condition est
vérifiée (tant qu'elle est vraie) : les boucles `while`.

Voici un petit exemple des boucles :

```codepython
nombre = 0

# Tant que `nombre` est inférieur à 5
# on répète le bout de code décalé à droite
while nombre < 5 :
    nombre = nombre + 1
    print("Mon nombre est égal à :", nombre)

print("À la fin, mon nombre est égal à :", nombre)
```

Dans cet exemple, l'ordinateur va répéter les instructions `nombre = nombre + 1`
et son affichage tant que la variable `nombre` est inférieure à 5.

{{% /box %}}

Dans notre Juste Prix, on veut que les instructions suivantes soient répétées
tant que le joueur n'a pas trouvé `nombre_aleatoire` (c'est-à-dire, tant que
`nombre` est différent à `nombre_aleatoire`) :

- Récupérer l'entrée du joueur ;
- Mettre à jour le score ;
- Vérifier l'entrée du joueur.

{{% box type="exercise" title="Bouclons !" %}}

À toi de rajouter la boucle `while` à l'endroit correspondant, de manière
d'avoir les bouts de code voulus dans notre boucle.

{{% /box %}}

---

C'est la fin ! Tu devrais avoir un Juste Prix fonctionnel et opérationnel pour
que Julie puisse y jouer !

Si tu le souhaites, tu peux faire des améliorations à ton jeu et lui ajouter des
fonctionnalités afin que Julie s'amuse plus. Voici quelques idées :

- Faire en sorte que le joueur ait un nombre précis d'essais à jouer avant de
perdre ;
- Faire en sorte que le joueur choisisse au début du jeu l'intervalle dans
lequel le nombre aléatoire doit être compris.

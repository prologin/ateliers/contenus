## Les opérateurs

Les opérateurs sont une notion essentielle en informatique, on les retrouve dans tous les langages de programmation. Comme leur nom l'indique, les opérateurs permettent de réaliser des opérations, aussi bien mathématiques que logiques.

Aujourd'hui, nous allons voir les opérateurs de comparaison : `==`, `!=`, `>`, `<`.

### **Les opérateurs de comparaison**

Les opérateurs de comparaison permettent de comparer deux nombres, et renvoient une valeur booléenne (vrai ou faux).

|Opérateur|Exemple|Résultat|
|---|---|---|
|Égal : `==`| `5 == 5` <br/> `1 == 2`|`True` <br/> `False`|
|Différent : `!=`|`1 != 2` <br/> `5 != 5`|`True` <br/> `False`|
|Supérieur :`>`|`2 > 1` <br/> `1 > 20`|`True` <br/> `False`|
|Inférieur :`<`|`1 < 2` <br/> `20 < 1`|`True` <br/> `False`|

```codepython
# Égal
print(5 == 5)
print(1 == 2)

# Différent
print(1 != 2)
print(5 != 5)

# Supérieur
print(2 > 1)
print(1 > 20)

# Inférieur
print(1 < 2)
print(20 < 1)
```

### **Utilisation des opérateurs avec les conditions**

Grâce à ces opérateurs, il est possible de créer des **conditions** capables de vérifier si une expression est vraie ou fausse. Voici un exemple :

```codepython
if 10 > 0:
    print("C'est vrai")
else:
    print("C'est faux")
```

Si l'affirmation écrite après `if` est **vraie**, alors Python va exécuter le code après le `if`.
Sinon, il exécutera le code après le `else`.

{{% box type="exercise" title="À exécuter" %}}

```codepython
nombre = 100

if nombre > 5:
	print("Le nombre est grand")
	nombre = -1
else:
	nombre = 1

print("Le nombre final est", nombre)
```

{{% /box %}}

Il est également possible de mettre plusieurs conditions à la suite avec le mot clé `elif`, qui est une contraction de "else if", et qu'on traduirait en français par "sinon, si ...".

```codepython
if 14 < 13:                         # Si 14 est inférieur à 13,
    print("14 < 13 est vrai")       # On fait ceci !
elif 5 != 3:                        # Sinon, si 5 est différent de 3,
    print("5 != 3 est vrai")        # On fait cela !
else:                               # Sinon,
    print("Les conditions précédentes sont fausses") # On fait ça
```

{{% box type="warning" title="Attention" %}}

Quand tu utilises les mots clés `if`, `else`, `elif`, il faut faire attention à bien *indenter* les instructions qui sont concernées ensuite.

{{<figure src="resources/images/if-elif.png" width=90% alt="if elif else">}}

Cela veut dire qu'il faut les décaler à droite, en utilisant un symbole *tabulation*.

{{<figure src="resources/images/tabulation.png" alt="tab key">}}

{{% /box %}}

Avec ces notions de base, tu vas pouvoir te lancer dans la réalisation du jeu du juste prix !

# Quelques notions pratiques

## Afficher du texte

Afin d'afficher des informations pour le joueur nous sera très utile ! On peut
dire à l'ordinateur d'écrire du texte. Pour cela, tu peux utiliser
l'instruction suivante :

```codepython
print("Hello world!")
```

{{% box type="warning" title="On peut lancer le code ?" %}}

Dans les boîtes noires, tu peux lancer le code écrit à l'intérieur ! Pour cela,
le bouton `Run`.

Tu peux également modifier le code à l'intérieur ! N'hésite surtout pas à
modifier ce que tu as pour avoir un autre comportement !

{{% /box %}}

Ici, notre programme est constitué d'une instruction : il s'agit d'un `print`
(*imprimer* en 🇫🇷). Cela permet d'afficher à l'écran le texte spécifié
entre les guillemets `"`.

{{% box type="exercise" title="Mini-mission 1 : Allo ?"%}}

Julie est dans l'immeuble d'en face et tu lui souhaites envoyer un message !
Vous aviez mis en place un système pour communiquer entre vous avec du code
Python ! Essaye de lui envoyer "Chicken attack!". Pour cela, tu peux utiliser
la fonction `print()`.

```codepython
# Affiche "Chicken attack!" après cette ligne !
```

{{% /box %}}

## Lire du texte

En Python, tu peux également récupérer du texte en demandant à l'utilisateur
de rentrer une phrase. Tu peux utiliser le code suivant :

```codepython
input("Quel jour est-il ?")
```

Tout comme la fonction `print()`, tu peux demander à l'ordinateur d'afficher
un texte avant de demander à l'utilisateur une entrée.

{{% box type="exercise" title="Mini-mission 2 : Réponse !"%}}

Julie essaye de te répondre à l'oral mais elle est trop loin... Pour remédier à
ce problème, lui demander une entrée pour comprendre ce qu'elle souhaite te
dire ! Essaye de lui demander une entrée avec comme message "Keske t'as dit ?".
Pour cela, tu peux utiliser la fonction `input()`.

```codepython
# Demande à Julie "Keske t'as dit ?" après cette ligne !
```

{{% /box %}}


L'instruction `input` (*entrée* en 🇫🇷) nous permet de faire cette demande à
l'utilisateur. Cependant, on ne peut pas la réutiliser. Pour cela, nous devons
stocker la réponse de l'utilisateur dans une variable.

## Variables

Une **variable** est un élément qui associe un nom à une valeur.

```python
ma_variable = 2
```

On peut lui donner n'importe quel nom :
```python
variable = 1
toto = 10
julie = 100000
voiture = -1
toto_et_tata_et_tous_les_autres = 5
```

On peut ensuite utiliser cette variable dans le reste du programme, en utilisant son nom :
```codepython
age = 10
print(age * 2)
print(age + 5)
```

### Les types

Une variable peut avoir une valeur de différents types :
- Un nombre entier : `ma_variable = 5`
- Une chaîne de caractères : `ma_variable = "bonjour !"` (on reconnait une telle variable grâce aux guillemets)
- Un nombre à virgule : `ma_variable = 1.05` (on utilise un point et non une virgule)
- Un booléen : `ma_variable = True` (deux valeurs possibles : `True` ou `False`, vrai ou faux, bien penser à mettre la majuscule !)

{{% box type="warning" title="Attention" %}}

Il ne faut pas mélanger les types : `ma_variable = 5` est le nombre entier 5 tandis que `ma_variable = "5"` est une chaîne de caractères contenant le chiffre 5.
Par contre, c'est utile de pouvoir passer de l'un à l'autre, et très simple à faire :

```python
mon_nombre = 5
ma_chaine = str(5)

mon_autre_chaine = "4"
mon_autre_nombre = int("4")
```

Les instructions `int` et `str` permettent de transformer le type d'une variable en un autre.

{{% /box %}}

Tu peux maintenant stocker la réponse à une instruction `input` dans une variable :

```codepython
prenom = input("Comment t'appelles-tu ?")
print(prenom)
```

L'instruction `input` renvoie le texte entré par l'utilisateur.
Et nous stockons ce texte dans la variable `prenom`, qui est ensuite affiché grâce à l'instruction `print`.


{{% box type="info" title="Info" %}}

Dans une instruction `print`, la virgule permet d'afficher plusieurs éléments les uns à la suite des autres en les séparant par un espace.

```codepython
prenom = input("Comment t'appelles-tu ?")
print("Tu t'appelles", prenom)
```

Si tu lances ce programme et réponds à la question, tu verras que le contenu de la variable `prenom` s'est rajouté à la suite de la chaîne de caractères "Tu t'appelles".

{{% /box %}}

{{% box type="warning" title="Attention" %}}

```codepython
age_text = input("Quel age as-tu ?")
print("J'ai", age_text, "ans.")
age_entier = int(age_text)
print("J'ai toujours", age_entier, "ans.")
age_soeur = age_entier + 2
print("Ma soeur a 2 ans de plus, elle a", age_soeur, "ans.")
```

On peut uniquement faire des opérations mathématiques, tel que `+` sur des nombres.

Donc nous devons **convertir** la chaine de caractères, stockée dans `age_text`, en entier, avec l'instruction `int()`.

{{% /box %}}